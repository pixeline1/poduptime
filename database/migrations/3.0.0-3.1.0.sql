ALTER TABLE pods ADD metalocation text;
ALTER TABLE pods ADD metanodeinfo text;
ALTER TABLE pods ADD owner text;
ALTER TABLE pods ADD metaimage text;
ALTER TABLE pods ADD onion text;
ALTER TABLE pods ADD i2p text;
ALTER TABLE pods ADD pp text;
ALTER TABLE pods ADD terms text;
ALTER TABLE pods ADD support text;
ALTER TABLE pods ADD camo text;
ALTER TABLE pods ADD zipcode text;

