ALTER TABLE pods ADD services jsonb;
ALTER TABLE pods DROP COLUMN service_facebook, DROP COLUMN service_wordpress, DROP COLUMN service_tumblr, DROP COLUMN service_twitter;
ALTER TABLE pods ADD podmin_notify_level int DEFAULT 50;
