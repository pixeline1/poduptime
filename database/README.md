# Database

## New install

When setting up a new install, import [`db/tables.sql`] and do not perform any migrations!

## Migrating

If you are upgrading your existing installation, execute the necessary migrations scripts found in [`db/migrations`].

[`db/tables.sql`]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/tables.sql
[`db/migrations`]: https://git.feneas.org/diasporg/Poduptime/tree/master/db/migrations
[`db/pods_apiv1.sql`]: https://git.feneas.org/diasporg/Poduptime/blob/master/db/pods_apiv1.sql
