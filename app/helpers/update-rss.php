<?php

/**
 * Create a basic sitemap.xml file to help site get indexed
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

//new servers found rss
file_put_contents($_SERVER['BASE_DIR'] . '/' . $_SERVER['NEW_SERVERS_RSS'], updateRSS());

podLog('RSS feed updated');
addMeta('rssmap_updated');
