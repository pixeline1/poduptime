<?php

/**
 * backup database
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

backupData();

podLog('backup ran');
addMeta('backup');
