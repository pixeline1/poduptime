<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

addMeta('pods_updating', false);
