<?php

/**
 * Get token to allow server editing.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException;

require_once __DIR__ . '/../../boot.php';

($_domain = $_GET['domain'] ?? null) || die('no pod domain given');

try {
    $pod = R::findOne('pods', 'domain = ?', [$_domain]);
    $pod || die('domain not found');
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $pod['token']       = bin2hex(random_bytes(9));
    $pod['tokenexpire'] = date('Y-m-d H:i:s', time() + 8700);
    R::store($pod);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
} catch (Exception $e) {
}
$link    = sprintf('https://%1$s/edit&domain=%2$s&token=%3$s', $_SERVER['HTTP_HOST'], $_domain, $pod['token']);
$expires = 'Expires: ' . $pod['tokenexpire'];
if ($pod['email']) {
    sendEmail($pod['email'], $t->trans('admin.tokenemailsubject'), $t->trans('admin.tokenemailbody', ['%(expires)' => $expires]) . "<br><br><a href='$link'>$link</a>");
    podLog('Key for owner generated and sent', $_domain);
    echo $t->trans('admin.tokensuccess');
} else {
    echo $t->trans('admin.register');
}
