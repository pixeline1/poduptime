<?php

/**
 * Show detailed pod stats.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException;

// Required parameters.
($_domain = $_GET['domain'] ?? null) || die('no domain given');

require_once __DIR__ . '/../../boot.php';

$sql = "
    SELECT
        to_char(date_checked, 'yyyy MM') AS yymm,
        count(*) AS total_checks,
        round(avg(total_users)) AS users,
        round(avg(local_posts)) AS local_posts,
        round(avg(comment_counts)) AS comment_counts
    FROM checks
    WHERE domain = ?
    GROUP BY yymm
    ORDER BY yymm
";

try {
    $totals = R::getAll($sql, [$_domain]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
?>
<div class="chart-container p-1 d-flex w-100">
    <canvas class="d-flex w-100" id="pod_chart_counts"></canvas>
</div>
<script>
    /**
     * Add a new chart for the passed data.
     *
     * @param id   HTML element ID to place the chart.
     * @param data Data to display on the chart.
     */
    new Chart(document.getElementById('pod_chart_counts'), {
        type: "line",
        data: {
            labels: <?php echo json_encode(array_column($totals, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($totals, 'users')); ?>,
                label: 'Users',
                fill: false,
                yAxisID: "l2",
                borderColor: "#A07614",
                backgroundColor: "#A07614",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($totals, 'local_posts')); ?>,
                label: 'Local Posts',
                fill: false,
                yAxisID: "l2",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($totals, 'comment_counts')); ?>,
                label: 'Comments',
                fill: false,
                yAxisID: "l2",
                borderColor: "#cecaa7",
                backgroundColor: "#cecaa7",
                borderWidth: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                l2: {
                    position: "left"
                }
            },
        }
    });
</script>
