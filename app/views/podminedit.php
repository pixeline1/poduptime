<?php

/**
 * Form to edit an existing pod.
 */

declare(strict_types=1);

?>

<form id="podminedit" action="/">
    <div class="form-group row">
        <label for="domain-input" class="col-2 ol-form-label"><?php echo $t->trans('admin.domain') ?> *</label>
        <div class="col-md-6">
            <div class="input-group mb-2 me-sm-2 mb-sm-0">
                <div class="input-group-addon"></div>
                <input type="text" id="domain-input" name="domain" class="form-control" placeholder="domain.com" aria-describedby="domain-help" aria-required="true" required>
            </div>
            <small id="domain-help" class="form-text text-muted"><?php echo $t->trans('admin.domainnote') ?>.</small>
        </div>
    </div>
    <br>
    <input type="hidden" name="gettoken">
    <button type="submit" class="btn btn-primary"><?php echo $t->trans('base.general.submit') ?></button>
</form>
