<?php

/**
 * get data for showfull.php calls.
 */

declare(strict_types=1);

use Carbon\Carbon;
use Poduptime\PodStatus;

require_once __DIR__ . '/../../boot.php';

$iso = new Matriphe\ISO639\ISO639();
$country_code = ipLocation($_SERVER['REMOTE_ADDR'])['country'];
$pods = allServersList($_GET['software']);

foreach ($pods as $pod) {
    $humanmonitored = Carbon::now()->subDays($pod['daysmonitored'])->diffForHumans(null, true);
    $last_podcheck  = Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_laststats'])->diffForHumans(null, true);
    $tip            = "{$pod['domain']} last checked $last_podcheck ago, over the last $humanmonitored uptime was {$pod['uptime_alltime']}%";
    if (($_COOKIE['domain'] ?? null) === $pod['domain']) {
        echo '<tr><td title="' . $t->trans('base.strings.list.cookienotice') . ' ' . $tip . '" data-placement="bottom" data-toggle="tooltip" class="bg-blue"><a class="text-white url" target="_pod" href="/go&domain=' . $pod['domain'] . '">' . idn_to_utf8($pod['domain']) . '</a><a href="/' . $pod['domain']  . '"><span style="font-size: 1.4em; opacity: 0.2;" class="fa fa-info-circle align-top ps-3"></span></a></td>';
    } else {
        echo '<tr><td data-placement="bottom" title="' . $tip . '" data-toggle="tooltip"><a class="url" target="_pod" href="/go&domain=' . $pod['domain'] . '">' . idn_to_utf8($pod['domain']) . '</a><a href="/' . $pod['domain']  . '"><span style="font-size: 1.4em; opacity: 0.2;" class="fa fa-info-circle align-top ps-3"></span></a></td>';
    }
    if ($pod['shortversion'] > $pod['masterversion']) {
        $version = $pod['shortversion'];
        $pre     = $t->trans('base.strings.list.devcode');
    } elseif (!$pod['shortversion']) {
        $version = '';
        $pre     = $t->trans('base.strings.list.unknowncode');
    } else {
        $version = $pod['shortversion'];
        $pre     = $t->trans('base.strings.list.productioncode');
    }
    $classver = 'green';
    if (version_compare($pod['shortversion'] ?? '', $pod['masterversion'] ?? '', '=')) {
        $classver = 'black';
    } elseif (version_compare($pod['shortversion'] ?? '', $pod['masterversion'] ?? '', '<')) {
        $classver = 'text-danger';
    }
    echo '<td>' . $pod['name'] . '</td>';
    $lastupdated = ($pod['date_updated'] ? Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_updated'])->diffForHumans() : 'unknown');
    echo '<td class="' . $classver . '"><div title="' . $pre . ' version: ' . $pod['shortversion'] . ' master version is: ' . ($pod['masterversion'] ?: 'unknown') . '" data-toggle="tooltip">' . $version . '</div></td>';
    echo '<td>' . $pod['softwarename'] . '</td>';
    $protocols = json_decode($pod['protocols'] ?? '[]') ?: [];
    echo '<td class="text-truncate">';
    foreach (c('softwares') as $protocolslist => $protocols_list) {
        if (isset($protocols_list['protocol'])) {
            foreach ($protocols as $protocol) {
                if ($protocols_list['text'] == $protocol) {
                    printf(
                        '<div class="smlogo me-sm-1 fa %1$s" title="%2$s" data-toggle="tooltip"><div class="hidden">%3$s</div></div>',
                        $protocols_list['faclass'],
                        $protocols_list['title'],
                        $protocol
                    );
                }
            }
        }
    }
    echo '</td>';
    echo '<td><a class="norightclick" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-url="app/views/podstat-uptime.php?domain=' . $pod['domain'] . '" data-title="' . $t->trans('base.strings.list.uptimestats', ['%(domain)' => $pod['domain']]) . '">' . ($pod['uptime_alltime'] > 0 ? $pod['uptime_alltime'] . '%' : '') . '</a></td>';
    echo '<td>' . (!empty($pod['ipv6']) ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</td>';
    echo '<td>' . ($pod['latency'] > 0 ? $pod['latency'] : '') . '</td>';
    echo '<td>' . ($pod['signup'] ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</td>';
    echo '<td><a class="norightclick" data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-url="app/views/podstat-counts.php?domain=' . $pod['domain'] . '" data-title="' . $t->trans('base.strings.list.userstats', ['%(domain)' => $pod['domain']]) . '">' . ($pod['total_users'] > 0 ? $pod['total_users'] : '') . '</a></td>';
    echo '<td>' . ($pod['active_users_halfyear'] > 0 ? $pod['active_users_halfyear'] : '') . '</td>';
    echo '<td>' . ($pod['active_users_monthly'] > 0 ? $pod['active_users_monthly'] : '') . '</td>';
    echo '<td>' . ($pod['local_posts'] > 0 ? $pod['local_posts'] : '') . '</td>';
    echo '<td>' . ($pod['comment_counts'] > 0 ? $pod['comment_counts'] : '') . '</td>';
    echo '<td><div title="This pod has been monitored ' . $humanmonitored . '" data-toggle="tooltip">' . $pod['monthsmonitored'] . '</div></td>';
    echo '<td><div>' . $pod['score'] . '</div></td>';
    echo '<td><div>' . ($pod['status'] ? $t->trans('base.general.up') : $t->trans('base.general.down')) . '</div></td>';
    echo '<td>' . ($pod['dnssec'] ? $t->trans('base.general.yes') : $t->trans('base.general.no')) . '</td>';
    if ($country_code === $pod['country']) {
        echo '<td class="text-success" data-toggle="tooltip" title="Country: ' . ($pod['countryname'] ?? '') . '"><b>' . $pod['country'] . '</b></td>';
    } else {
        echo '<td data-toggle="tooltip" title="Country: ' . ($pod['countryname'] ?? 'n/a') . '">' . $pod['country'] . '</td>';
    }
    echo '<td>' . $pod['city'] . '</td>';
    echo '<td>' . $pod['state'] . '</td>';
    echo '<td data-toggle="tooltip" title="' . ($pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : '') . '">' . ($pod['detectedlanguage'] ? strtoupper($pod['detectedlanguage']) : '') . '</td>';
    echo '<td class="text-truncate">';
    $services = json_decode($pod['services'] ?? '[]') ?: [];
    ($pod['service_xmpp'] ?? false) && $services[] = 'xmpp';
    foreach (c('softwares') as $servicelist => $services_list) {
        if (isset($services_list['service'])) {
            foreach ($services as $service) {
                if ($services_list['text'] == $service) {
                    printf(
                        '<div class="smlogo me-sm-1 fa %1$s" data-toggle="tooltip" title="%2$s"><div class="hidden">%3$s</div></div>',
                        $services_list['faclass'],
                        $services_list['title'],
                        $service
                    );
                }
            }
        }
    }
    echo '</td>';
    $podmin_statement = htmlentities($pod['podmin_statement'] ?? '', ENT_QUOTES);
    echo '<td>' . ($podmin_statement ? '<a data-bs-toggle="modal" data-bs-target="#myModal" href="#" data-title="Server admin statement" data-message="<p>' . $podmin_statement . '</p>">&#128172;</a>' : '&nbsp;') . '</td></tr>';
}
?>
<script>
$(document).ready(function () {
$(".norightclick").on("contextmenu", function () {
            return false;
        });
});
</script>
