<div class="container">
    <h1 class="text-center"><?php echo $t->trans('base.strings.stats.title') ?></h1>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-25 text-center">
            <h4><?php echo $t->trans('base.strings.stats.users') ?></h4>
        </div>
        <div class="d-flex w-25 text-center">
            <h4><?php echo $t->trans('base.strings.stats.serverss') ?></h4>
        </div>
        <div class="d-flex w-25 text-center">
            <h4><?php echo $t->trans('base.strings.stats.serversc') ?></h4>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-25 text-center">
            <canvas class="d-flex" id="total_network_users"></canvas>
        </div>
        <div class="d-flex w-25 text-center">
            <canvas class="d-flex" id="total_network_pods"></canvas>
        </div>
        <div class="d-flex w-25 text-center">
            <canvas class="d-flex" id="total_servers_country"></canvas>
        </div>
    </div>
    <br>
    <h1 class="text-center p-1"><?php echo $t->trans('base.strings.stats.statsfor') . ' ' ?> <?php echo $software_all ?></h1>
        <div class="row justify-content-center p-1">
            <div class="d-flex w-100 justify-content-center">
                <h4><?php echo $t->trans('base.strings.stats.average') ?> <?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.growth') ?></h4>
            </div>
            <div class="d-flex w-100 justify-content-center">
                <canvas class="d-flex w-100 justify-content-center" id="user_growth"></canvas>
            </div>
        </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 justify-content-center">
            <h4><?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.serversper') ?></h4>
        </div>
        <div class="d-flex w-100 justify-content-center">
            <canvas class="d-flex w-100 justify-content-center" id="pod_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 justify-content-center">
            <h4><?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.commentsper') ?></h4>
        </div>
        <div class="d-flex w-100 justify-content-center">
            <canvas class="d-flex w-100 justify-content-center" id="comment_growth"></canvas>
        </div>
    </div>
    <div class="row justify-content-center p-1">
        <div class="d-flex w-100 justify-content-center">
            <h4><?php echo $software_all ?> <?php echo $t->trans('base.strings.stats.postsper') ?></h4>
        </div>
        <div class="d-flex w-100 justify-content-center">
            <canvas class="d-flex w-100 justify-content-center" id="posts_growth"></canvas>
        </div>
    </div>
</div>
