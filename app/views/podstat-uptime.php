<?php

/**
 * Show detailed pod stats.
 */

declare(strict_types=1);

use RedBeanPHP\R;
use RedBeanPHP\RedException;

// Required parameters.
($_domain = $_GET['domain'] ?? null) || die('no domain given');

require_once __DIR__ . '/../../boot.php';

$sql = "
    SELECT
        to_char(date_checked, 'yyyy MM') AS yymm,
        count(*) AS total_checks,
        round(avg(online::INT),4)*100 AS uptime,
        round(avg(latency),2) * 1000 AS latency
    FROM checks
    WHERE domain = ?
    GROUP BY yymm
    ORDER BY yymm
";

try {
    $totals = R::getAll($sql, [$_domain]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
?>
<div class="chart-container p-1 d-flex w-100">
    <canvas id="pod_chart_responses"></canvas>
</div>
<script>/**
 * Add a new chart for the passed data.
 *
 * @param id   HTML element ID to place the chart.
 * @param data Data to display on the chart.
 */
    new Chart(document.getElementById('pod_chart_responses'), {
        type: "line",
        data: {
            labels: <?php echo json_encode(array_column($totals, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($totals, 'uptime')); ?>,
                label: 'Uptime %',
                fill: false,
                yAxisID: "l1",
                borderColor: "#A07614",
                backgroundColor: "#A07614)",
                borderWidth: 4,
                pointHoverRadius: 11
            }, {
                data: <?php echo json_encode(array_column($totals, 'latency')); ?>,
                label: 'Latency ms',
                fill: false,
                yAxisID: "r1",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 3,
                pointHoverRadius: 11,
                pointStyle: 'rect'
            }]
        },
        options: {
            responsive: true,
            interaction: {
                mode: 'index',
                intersect: false,
            },
            stacked: false,
            scales: {
                l1: {
                    type: 'linear',
                    display: true,
                    position: 'left',
                    min: 50,
                    max: 100,
                },
                r1: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        drawOnChartArea: true,
                    },
                },
            }
        },
    })

</script>
