<?php

/**
 * help users find a pod with Q&A wizard
 */

declare(strict_types=1);

use Poduptime\PodStatus;
use RedBeanPHP\R;

require_once __DIR__ . '/../../boot.php';

$page         = (int) ($_GET['page'] ?? 1);
$softwarename = $_GET['softwarename'] ?? null;
$country      = $_GET['country'] ?? null;
$users        = $_GET['users'] ?? null;

$iso = new Matriphe\ISO639\ISO639();

?>
<script>
    $(document).ready(function () {
        $('.wizardnext2').on('click', function () {
            var software = $('input:radio[name="software"]:checked').val();
            if (software) {
                $.get('app/views/wizard.php?page=2&softwarename=' + software, function (html) {
                    $('.wizard').replaceWith(html);
                    $('table').trigger('refreshColumnSelector', ['columns', [3, 14, 18, 9]])
                });
            } else {
                $('.wizardnext2').text('Select software, then. Next..');
            }
        });
        $('.wizardnext3').on('click', function () {
            var columns = [];
            columns[3] = '<?php echo $softwarename; ?>';
            $('table').trigger('search', [columns])
            var country = $('input:radio[name="country"]:checked').val();
            if (country) {
                $.get('app/views/wizard.php?page=3&softwarename=<?php echo $softwarename; ?>&country=' + country, function (html) {
                    $('.wizard').replaceWith(html);
                });
            } else {
                $('.wizardnext3').text('Select Country, then. Next..');
            }
        });
        $('.wizardnext4').on('click', function () {
            var columns = [];
            columns[18] = '<?php echo $country; ?>';
            columns[3] = '<?php echo $softwarename; ?>';
            $('table').trigger('search', [columns])
            var users = $('input:text[name="users"]').val();
            if (users) {
                $.get('app/views/wizard.php?page=4&softwarename=<?php echo $softwarename; ?>&country=<?php echo $country; ?>&users=' + users, function (html) {
                    $('.wizard').replaceWith(html);
                });
            } else {
                $('.wizardnext4').text('Select Users, then. Next..');
            }
        });
        $('.suggestpod').on('click', function () {
            var columns = [];
            columns[3] = '<?php echo $softwarename; ?>';
            var month = $('input:text[name="month"]').val();
            if (month) {
                columns[14] = '>= ' + month;
            } else {
                columns[14] = '>= 0';
            }
            columns[18] = '<?php echo $country; ?>';
            var users = '<?php echo $users; ?>';
            if (users) {
                columns[9] = '>= ' + users;
            } else {
                columns[9] = '>= 0';
            }
            $('table').trigger('search', [columns])
            $('#myModal').modal('hide')
        });
        $('#month').ionRangeSlider();
    });
</script>
<div class="container wizard">
<?php
if ($page === 1) {
    echo '<div class="row m-1 p-1"><div class="col-6 p-2">' . $t->trans('base.strings.wizard.about') . ' <br>' . $t->trans('base.strings.wizard.recommend') . ' :</div><div class="col-8">';
    $softwares = c('softwares');
    foreach ($softwares as $software => $details) {
        if ($details['wizard'] === '1') {
            echo '<input type="radio" name="software" value="' . $software . '" aria-describedby="help"> ' . ucwords($software) . '<br>';
            echo '<p id="help" class="form-text text-muted">';
            echo $t->trans('softwares.' . $software);
            echo '</p>';
            echo '</label>';
        }
    }
    echo '</div></div><button type="button" class="wizardnext2 btn bg-grey">' . $t->trans('base.general.next') . '</button>';
} elseif ($page === 2) {
    $countries = data('country', $softwarename);
    // Sort country names correctly.
    uasort($countries, function ($c1, $c2) {
        return country($c1)->getName() <=> country($c2)->getName();
    });

    echo '<div class="row m-1 p-1"><div class="col-9">' . $t->trans('base.strings.wizard.country') . ' <br>' . $t->trans('base.strings.wizard.countrywhy') . ' </div><div class="col-8 p-3">';
    foreach ($countries as $country) {
        printf(
            '<label class="m-0"><input class="ml-2" type="radio" name="country" value="%1$s" /> %2$s %3$s</label><br>',
            $country,
            country($country)->getName(),
            country($country)->getEmoji()
        );
    }
    echo '</div></div><button type="button" class="wizardnext3 btn bg-grey">' . $t->trans('base.general.next') . '</button>';
} elseif ($page === 3) {
    $users   = data('total_users', $softwarename, $country);
    $umin    = min($users);
    $umax    = max($users);

    if ($umin === $umax && $users > 0) {
        echo '<div class="row m-1 p-1"><div class="col-9">' . $t->trans('base.strings.wizard.countrynone') . ' .</div><div class="col-8 p-3">';
        echo '<div class="row m-1 p-1"><div class="col-9"></div><div class="col-8">';
        printf('<input type="text" name="users" value="%d" hidden>', $umin);
    } else {
        echo '<div class="row m-1 p-1"><div class="col-9">' . $t->trans('base.strings.wizard.size') . ' <br>' . $t->trans('base.strings.wizard.sizenote') . ' </div><div class="col-8 p-3">';
        echo '<div class="row m-1 p-1"><div class="col-9">' . $t->trans('base.strings.wizard.usersmin') . ' :</div><div class="col-8">';
        printf('<input type="text" name="users" id="month" data-min="%d" data-max="%d" data-step="1">', $umin, $umax);
    }
    echo '</div></div><button type="button" class="wizardnext4 btn bg-grey">' . $t->trans('base.general.next') . '</button>';
} elseif ($page === 4) {
    podLog('wizard country ' . $country . ' software ' . $softwarename);
    echo '</div></div><button type="button" class="wizardlast suggestpod btn bg-grey">' . $t->trans('base.strings.wizard.suggest') . '</button>';
    echo '<div class="small wizardnote">' . $t->trans('base.strings.wizard.wizardfinal') . ' <br><br>' . $t->trans('base.strings.wizard.wizardnote') . ' </div>';
}

function data($column, $software = null, $country = null, $users = null)
{
    $sql    = "
        SELECT DISTINCT $column
        FROM pods
        WHERE status < ?
        AND signup
        AND softwarename = ?
    ";
    $params = [PodStatus::SYSTEM_DELETED, $software];

    if ($country) {
        $sql      .= ' AND country = ?';
        $params[] = $country;
    }
    if ($users) {
        $sql      .= ' AND total_users >= ?';
        $params[] = $users;
    }
    $data = R::getAll($sql, $params);
    return array_filter(array_column($data, $column));
}
