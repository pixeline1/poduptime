<?php

/**
 * Collection of functions for local use.
 */

declare(strict_types=1);

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\ProcessIdProcessor;
use Monolog\Processor\WebProcessor;
use Poduptime\AddServerTask;
use Poduptime\PodStatus;
use Poduptime\UpdateServerTask;
use RedBeanPHP\R;
use RedBeanPHP\RedException;
use Spatie\Async\Pool;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\PostgreSql;
use Spatie\DbDumper\Exceptions\DumpFailed;
use samdark\sitemap\Sitemap;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Laminas\Feed\Writer\Feed;
use Jaybizzle\CrawlerDetect\CrawlerDetect;
use CodeZero\BrowserLocale\BrowserLocale;

/**
* log things: type = debug, info, notice, warning, error, critical, alert, emergency
 */
function podLog($text, $domain = null, $type = 'info')
{
    $CrawlerDetect = new CrawlerDetect();
    if (!$CrawlerDetect->isCrawler()) {
        $log = new Logger('poduptime');
        $uptype = strtoupper($type);
        $log->pushHandler(new StreamHandler($_SERVER['LOG_DIRECTORY'] . '/' . $_SERVER['APP_ENV'] . '.log', "$uptype"));
        $log->pushProcessor(new WebProcessor());
        $log->pushProcessor(new IntrospectionProcessor());
        $log->pushProcessor(new ProcessIdProcessor());
        if (!isCli()) {
            $browser = new BrowserLocale($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
            $language = $browser->getLocale()->language;
            $log->$type($text, ['domain' => $domain, 'UA' => $_SERVER['HTTP_USER_AGENT'] ?? '', 'LANG' => $language ?? '']);
        } else {
            $log->$type($text, ['domain' => $domain]);
        }
    }
}

/**
 * Helper to get config array values.
 */
function c(string $param = null, mixed $default = null): mixed
{
    static $config;
    if ($config === null) {
        $config = require __DIR__ . '/../config/app.php';
        is_array($config) || die('Invalid config format.');
    }

    if ($param === null) {
        return $config;
    }

    if (array_key_exists($param, $config)) {
        return $config[$param];
    }

    return $default;
}

/**
 * Check if started from cli.
 */
function isCli(): bool
{
    if (defined('STDIN')) {
        return true;
    }

    if (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] === 'localhost') {
        return true;
    }

    if (PHP_SAPI === 'cli') {
        return true;
    }

    if (PHP_SAPI === 'cgi-fcgi') {
        return true;
    }

    if (array_key_exists('SHELL', $_SERVER)) {
        return true;
    }

    if (empty($_SERVER['REMOTE_ADDR']) && !isset($_SERVER['HTTP_USER_AGENT']) && count($_SERVER['argv']) > 0) {
        return true;
    }

    if (!array_key_exists('REQUEST_METHOD', $_SERVER)) {
        return true;
    }

    return false;
}

/**
 * Output a debug message and variable value
 */
function debug($label, $var = null, $dump = false)
{
    global $debug;

    if (!$debug) {
        return;
    }

    if ($dump || is_array($var)) {
        $output = print_r($var, true);
    } elseif (is_bool($var)) {
        $output = $var ? 'true' : 'false';
    } else {
        $output = (string) $var;
    }

    printf('%s: %s%s', $label, $output, "\n\n");
}

/**
 * Update meta table
 *
 */
function addMeta(string $name, mixed $value = '1'): int|string
{
    try {
        $u          = R::dispense('meta');
        $u['name']  = $name;
        $u['value'] = $value;
        return R::store($u);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

/**
 * Get meta table last item
 */
function getMeta(string $name, string $column = 'value'): mixed
{
    try {
        return R::getCell("
            SELECT $column
            FROM meta
            WHERE name = ?
            ORDER BY id DESC
            LIMIT 1
        ", [$name]);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
}

/**
 * Format Seconds to human for last update
 */
function secondsToTime(int $seconds, $format = '%3$d hours, %2$d minutes'): string
{
    $seconds = (int) abs($seconds);
    $minutes = floor($seconds / 60);
    $hours   = floor($minutes / 60);

    return sprintf($format, $seconds % 60, $minutes % 60, $hours);
}

/**
 * Convert text file to proper sql like query
 */
function txtToQuery(string $file): string
{
    return '%(' . preg_replace('/\R+/', "|", trim(file_get_contents($file))) . ')%';
}

/**
 * backup sql data to drive
 */
function backupData(): string
{
    $keep_for = 60 * 60 * 24 * (float) $_SERVER['BACKUP_RETENTION'];
    $backup_file = $_SERVER['BACKUP_DIRECTORY'] . '/tar_dump.' . date('Y.m.d.His') . '.sql.gz';

    try {
        PostgreSql::create()
        ->setDbName($_SERVER['DB_DATABASE'])
        ->setUserName($_SERVER['DB_USERNAME'])
        ->setPassword($_SERVER['DB_PASSWORD'])
        ->addExtraOption('--clean')
        ->addExtraOption('--no-owner')
        ->addExtraOption('--format=tar')
        ->useCompressor(new GzipCompressor())
        ->dumpToFile($backup_file);
    } catch (DumpFailed $e) {
        podLog('database dump failed' . $e, null, 'error');
    }

    $fileSystemIterator = new FilesystemIterator($_SERVER['BACKUP_DIRECTORY']);
    $fileSystemIterator->setFlags(FilesystemIterator::SKIP_DOTS);
    $now = time();
    foreach ($fileSystemIterator as $file) {
        if ($now - $file->getCTime() >= $keep_for) {
            unlink($_SERVER['BACKUP_DIRECTORY'] . '/' . $file->getFilename());
        }
    }
    return $backup_file;
}

/**
 * write to checks table from input array
 */
function writeCheck($array): bool
{
    try {
        $c                   = R::dispense('checks');
        $c['domain']         = $array[0] ?? null;
        $c['online']         = $array[1] ?? null;
        $c['error']          = $array[2] ?? null;
        $c['latency']        = $array[3] ?? null;
        $c['total_users']    = $array[4] ?? null;
        $c['local_posts']    = $array[5] ?? null;
        $c['comment_counts'] = $array[6] ?? null;
        $c['shortversion']   = $array[7] ?? null;
        $c['version']        = $array[8] ?? null;
        R::store($c);
        $success = true;
    } catch (\RedBeanPHP\RedException $e) {
        podLog('Error in SQL query' . $e->getMessage());
        $success = false;
    }
    return $success;
}

/**
 * read from checks table
 */
function readCheck($domain, $online): array
{
    try {
        $checkdata = R::findOne('checks', 'domain = ? AND online = ?', [$domain, $online]);
    } catch (\RedBeanPHP\RedException $e) {
        podLog('Error in SQL query' . $e->getMessage());
    }

    return [
        'date_checked' => $checkdata['date_checked'] ?? null,
        'version'      => $checkdata['version'] ?? null,
    ];
}

/**
 * Check if $_GET or $argv parameter is set
 */
function hasArgParam(string $param): bool
{
    global $argv;
    return isset($_GET[$param]) || in_array($param, $argv ?? [], true);
}

/**
* send email using sendmail from system install
 */
function sendEmail($to, $subject, $message, $cc = ''): bool
{
    $mail = new PHPMailer(true);

    try {
        $mail->isSMTP();
        $mail->Host       = $_SERVER['SMTP_HOSTNAME'];
        $mail->SMTPAuth   = true;
        $mail->Username   = $_SERVER['SMTP_USERNAME'];
        $mail->Password   = $_SERVER['SMTP_PASSWORD'];
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port       = $_SERVER['SMTP_PORT'] ?? 587;
        $mail->setFrom($_SERVER['ADMIN_EMAIL'], $_SERVER['TITLE']);
        $mail->addAddress($to);
        $cc && $mail->addCC($cc);
        $mail->isHTML(true);
        $mail->Subject = $subject . ' ' . $_SERVER['TITLE'];
        $mail->Body    = $message;
        $mail->AltBody = $message;
        $sent = $mail->send();
    } catch (Exception $e) {
        podLog("Message could not be sent. Mailer Error: $mail->ErrorInfo", $to, "error");
    }
    return $sent;
}

/**
 * update one server
 */
function updateServer(string $domain): bool
{
    $pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    $pool
        ->add(new UpdateServerTask($domain))
        ->catch(fn() => false);
    $pool->forceSynchronous()->wait();
    return true;
}

/**
 * add one server
 */
function addServer(string $domain): bool
{
    $pool = Pool::create()->concurrency((int) $_SERVER['PROCESS_LIMIT']);
    $pool
        ->add(new AddServerTask($domain))
        ->catch(fn() => false);
    $pool->forceSynchronous()->wait();
    return true;
}

/**
 * list of servers data for tables and maps et
 */
function allServersList($software = null, $latest = null)
{
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

    try {
        $sql = '
            SELECT domain, dnssec, podmin_statement, masterversion, shortversion, softwarename, daysmonitored, monthsmonitored, score, signup, protocols, name, country, countryname, city, state, detectedlanguage, uptime_alltime, active_users_halfyear, active_users_monthly, services, service_xmpp, latency, date_updated, ipv6, total_users, local_posts, comment_counts, status, date_laststats, lat, long, date_created, metatitle, metadescription
        FROM pods
        WHERE status < ? 
        AND score > 0
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
    ';
        if ($software) {
            $sql .= ' AND softwarename = ? ORDER BY weightedscore DESC';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains, $software]);
        } elseif ($latest) {
            $sql .= " AND score = 51 AND date_created > current_date - interval '2 hours' ORDER BY date_created DESC LIMIT 50";
            $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
        } else {
            $sql .= ' ORDER BY weightedscore DESC';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $hiddensoftwares, $hiddendomains]);
        }
    } catch (\RedBeanPHP\RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    return $pods;
}

/**
 * list of domains for other queries
 */
function allDomainsData($software = null, $count = false)
{
    try {
        if ($count) {
            $sql = '
            SELECT count(domain)
            ';
        } else {
            $sql = '
            SELECT domain
            ';
        }
        $sql .= '
        FROM pods
        WHERE status <= ? 
    ';
        if ($software) {
            $sql .= ' AND softwarename = ?';
            $pods = R::getAll($sql, [PodStatus::RECHECK, $software]);
        } else {
            $pods = R::getAll($sql, [PodStatus::RECHECK]);
        }
    } catch (\RedBeanPHP\RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    return $pods;
}

/**
 * Create sitemap
 */
function updateSitemap(): void
{
    $sitemap = new Sitemap($_SERVER['SITEMAP']);
    $pods = allDomainsData() ?? [];
    foreach (array_column($pods, 'domain') as $domain) {
        $sitemap->addItem('https://' . $_SERVER['DOMAIN'] . '/' . $domain, time(), Sitemap::HOURLY);
    }
    $sitemap->setStylesheet('https://www.sitemaps.org/schemas/sitemap/0.9');
    $sitemap->write();
}

/**
 * Create RSS feeds
 */
function updateRSS(): string
{
    $feed = new Feed();
    $feed->setTitle($_SERVER['TITLE']);
    $feed->setLink('https://' . $_SERVER['DOMAIN']);
    $feed->setFeedLink('https://' . $_SERVER['DOMAIN'] . '/' . $_SERVER['NEW_SERVERS_RSS'], 'atom');
    $feed->addAuthor([
        'name' => $_SERVER['TITLE'],
        'email' => $_SERVER['ADMIN_EMAIL'],
        'uri' => 'https://' . $_SERVER['DOMAIN'],
    ]);
    $feed->setDateModified(time());
    $feed->addHub('https://pubsubhubbub.appspot.com/');

    $servers = allServersList(null, true);

    foreach ($servers as $server) {
        $entry = $feed->createEntry();
        $entry->setTitle($server['softwarename']);
        $entry->setLink('https://' . $_SERVER['DOMAIN'] . '/' . $server['domain']);
        $entry->setDateModified(time());
        $entry->setDateCreated(time());
        $entry->setDescription($server['metatitle'] ?? $server['softwarename']);
        $entry->setContent('New ' . $server['softwarename'] . ' server found in the fediverse. '  . 'https://' . $_SERVER['DOMAIN'] . '/' . $server['domain']);
        $feed->addEntry($entry);
    }

    return $feed->export('atom');
}

/**
 * find closest servers by geo
 */
function closestServers($ip = null, $limit = 5, $software = '%')
{
    $iploc = ipLocation($ip ?? $_SERVER['REMOTE_ADDR']);

    $sql = "
    SELECT domain, softwarename, city, state, countryname,
    ST_Distance(ST_MakePoint(long::numeric, lat::numeric)::geography, ST_MakePoint(:long, :lat)::geography) AS postgis_distance,
    earth_distance(ll_to_earth(:lat, :long), ll_to_earth (lat::numeric, long::numeric)) AS earth_distance,
    (point(long::numeric, lat::numeric) <@> point(:long, :lat)) * 1609.344 AS point_distance
    FROM
    pods
    WHERE
    score = 100
    AND status < :status
    AND monthsmonitored > 5
    AND uptime_alltime > 99
    AND signup = 'true'
    AND total_users > 1
    AND softwarename SIMILAR TO :software
    AND softwarename NOT SIMILAR TO :hiddensoftwares
    AND domain NOT SIMILAR TO :hiddendomains
    AND earth_box(ll_to_earth (:lat, :long), 5000000) @> ll_to_earth (lat::numeric, long::numeric)
    AND earth_distance(ll_to_earth (:lat, :long), ll_to_earth (lat::numeric, long::numeric)) < 5000000
    ORDER BY
    earth_distance, monthsmonitored, uptime_alltime
    LIMIT :limit
    ";
    $hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
    $hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

    try {
        $closest = R::getAll($sql, [
            ':status' => PodStatus::RECHECK,
            ':hiddensoftwares' => $hiddensoftwares,
            ':software' => $software,
            ':hiddendomains' => $hiddendomains,
            ':lat' => $iploc['lat'],
            ':long' => $iploc['long'],
            ':limit' => $limit
        ]);
    } catch (RedException $e) {
        die('Error in SQL query: ' . $e->getMessage());
    }
    return $closest;
}
